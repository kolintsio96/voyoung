$(document).ready(function(){
	var $trackListItem = $('.track-list__item');
	$('.track-list__link').hover(function(){
		var indexItem = $trackListItem.index($(this).closest('.track-list__item'));
		$trackListItem.each(function(index, item) {
			var className = 'main__circle_' + (index+1);
				console.log(className);
			if(index <= indexItem) {
				
			
				if(!$('.main__circle').hasClass(className)){
					$('.main__circle').addClass(className)
				}
			} else {
				$('.main__circle').removeClass(className)
			}
		})
		
	}, function(){
		$trackListItem.each(function(index, item) {
			var className = 'main__circle_' + (index+1)
			// $('.main__circle').removeClass(className)
		})
	});	
	
	$('#fullpage').fullpage({
		menu: '#menu',
		anchors:['Main', 'News',"Our-projects","Gallery","About","Events"],
		navigationTooltips: ['Main', 'News'],
		css3: true,
		scrollingSpeed: 700,
		easing: 'easeInOutCubic',
		easingcss3: 'ease',
	});
	$('.slider').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: true,
	  infinite: true,
	  pauseOnHover:true,
  	  autoplaySpeed: 10000,
	  arrows: false,
	  fade: true,
	  asNavFor: '.slider-left'
	});
	$('.slider-left').slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  infinite: true,
	  asNavFor: '.slider',
	  vertical: true,
	  dots: false,
	  arrows: true,
	  centerMode: true,
	  focusOnSelect: true
	});
	$('.slider-third').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: true,
	  infinite: true,
	  pauseOnHover:true,
  	  autoplaySpeed: 10000,
	  arrows: false,
	  fade: false,
	  asNavFor: '.third-small'
	});
	$('.third-small').slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  infinite: true,
	  asNavFor: '.slider-third',
	  vertical: false,
	  dots: false,
	  arrows: false,
	  centerMode: true,
	  focusOnSelect: true
	});
	$('.galery-slider').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: true,
	  infinite: true,
	  pauseOnHover:true,
  	  autoplaySpeed: 3000,
	  arrows: true,
	});
	$('.about-slider').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: true,
	  infinite: true,
	  pauseOnHover:true,
  	  autoplaySpeed: 5000,
	  arrows: true,
	  fade:true,
	});
	$('.news-slider').slick({
	  dots: false,
	  infinite: true,
	  slidesToShow: 2,
	  slidesToScroll: 1,
	  autoplay: true,
	  speed: 300,
  	  autoplaySpeed: 5000,
	  arrows: true,
	  pauseOnHover:true,
	});
	$('.product-slider').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  fade: true,
	  arrows: false,
	  asNavFor: '.product-slider-small'
	});
	$('.product-slider-small').slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  infinite: true,
	  asNavFor: '.product-slider',
	  vertical: true,
	  dots: false,
	  arrows: false,
	  centerMode: true,
	  focusOnSelect: true
	});

	function changeImgColor() {
		if($('#product__female').prop('checked') && $('#product-color').prop('checked')){
			$('.product-slider__item .zoo-item').each(function(){
				var img_src = $(this).attr('data-zoo-image');
				img_src = img_src.replace('white','black');
				img_src = img_src.replace('male','woman');
				console.log(img_src);
				$('.zoo-img').css('background-image','url("' + img_src + '")');
			});

			$('.product-slider-small__item img').each(function(){
				var img_src = $(this).attr('src');
				img_src = img_src.replace('white','black');
				console.log(img_src);
				$(this).attr('src',img_src);
			});
		} else if($('#product__female').prop('checked') && $('#product-color').prop('checked')==false){
			$('.product-slider__item .zoo-item').each(function(){
				var img_src = $(this).attr('data-zoo-image');
				img_src = img_src.replace('black','white');
				img_src = img_src.replace('male','woman');
				console.log(img_src);
				$('.zoo-img').css('background-image','url("' + img_src + '")');
			});

			$('.product-slider-small__item img').each(function(){
				var img_src = $(this).attr('src');
				img_src = img_src.replace('black','white');
				console.log(img_src);
				$(this).attr('src',img_src);
			});
		}else if($('#product-color').prop('checked') && $('#product__female').prop('checked')==false){
			$('.product-slider__item .zoo-item').each(function(){
				var img_src = $(this).attr('data-zoo-image');
				img_src = img_src.replace('white','black');
				img_src = img_src.replace('woman','male');
				console.log(img_src);
				$('.zoo-img').css('background-image','url("' + img_src + '")');
			});

			$('.product-slider-small__item img').each(function(){
				var img_src = $(this).attr('src');
				img_src = img_src.replace('white','black');
				console.log(img_src);
				$(this).attr('src',img_src);
			});
		} else if($('#product-color').prop('checked')==false && $('#product__female').prop('checked')==false){
			$('.product-slider__item .zoo-item').each(function(){
				var img_src = $(this).attr('data-zoo-image');
				img_src = img_src.replace('black','white');
				img_src = img_src.replace('woman','male');
				console.log(img_src);
				$('.zoo-img').css('background-image','url("' + img_src + '")');
			});

			$('.product-slider-small__item img').each(function(){
				var img_src = $(this).attr('src');
				img_src = img_src.replace('black','white');
				console.log(img_src);
				$(this).attr('src',img_src);
			});
		}
	}
	$('.product-color__label').on('click', function() {
		changeImgColor();
	});
	// function zoomImage(){
	// 	var productZoom = document.querySelector('.product-slider__item');
	// 	var paneContainer = document.querySelector('.product-slider');

	// 	new Drift(productZoom, {
	// 	  paneContainer: paneContainer,
	// 	  zoomFactor: 1.5,
	// 	  showWhitespaceAtEdges:false,
	// 	  inlinePane: false
	// 	});
	// }
	//zoomImage();
	setTimeout(function(){
		$('.about-slider .slick-prev').on('click',function(){
			$(this).addClass('arrow-left-about');
			setTimeout(function(){
				$('.about-slider .slick-prev').removeClass('arrow-left-about');
			},500)
		});
		$('.about-slider .slick-next').on('click',function(){
			$(this).addClass('arrow-right-about');
			setTimeout(function(){
				$('.about-slider .slick-next').removeClass('arrow-right-about');
			},500)
		});
		$('.galery-slider .slick-prev').on('click',function(){
			$(this).addClass('arrow-left-galery');
			setTimeout(function(){
				$('.galery-slider .slick-prev').removeClass('arrow-left-galery');
			},500)
		});
		$('.galery-slider .slick-next').on('click',function(){
			$(this).addClass('arrow-right-galery');
			setTimeout(function(){
				$('.galery-slider .slick-next').removeClass('arrow-right-galery');
			},500)
		});
		$('.slider-left .slick-prev').on('click',function(){
			$(this).addClass('arrow-top-news');
			setTimeout(function(){
				$('.slider-left .slick-prev').removeClass('arrow-top-news');
			},500)
		});
		$('.slider-left .slick-next').on('click',function(){
			$(this).addClass('arrow-bottom-news');
			setTimeout(function(){
				$('.slider-left .slick-next').removeClass('arrow-bottom-news');
			},500)
		});
		$('.news-slider .slick-prev').on('click',function(){
			$(this).addClass('arrow-left-news');
			setTimeout(function(){
				$('.news-slider .slick-prev').removeClass('arrow-left-news');
			},500)
		});
		$('.news-slider .slick-next').on('click',function(){
			$(this).addClass('arrow-right-news');
			setTimeout(function(){
				$('.news-slider .slick-next').removeClass('arrow-right-news');
			},500)
		});
	},2000);
	$('.categories').hover(function(){
		$(this).addClass('active');
	},function(){
		$(this).removeClass('active');
	});
	$('.categories:last-child').hover(function(){
		$(this).addClass('active_3');
	},function(){
		$(this).removeClass('active_3');
	});
	(function zoomImage(){
		$('.zoo-item').ZooMove({
            cursor: 'true',
            scale: '1.5',
        });
	}());
});
