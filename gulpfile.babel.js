'use strict';
import gulp from "gulp";
import rename from "gulp-rename";
import sass from "gulp-ruby-sass";
import sourceMaps from "gulp-sourcemaps";
import uglify from 'gulp-uglify';
import cleanCSS from "gulp-clean-css";
import del from 'del';
import autoPrefix from "gulp-autoprefixer";
import babel from "gulp-babel";
import browserSync from "browser-sync";

const
    reload = browserSync.reload,
    paths = {
        styles: './src/styles/index.scss',
        all_style: './src/**/*.scss',
        scripts: './src/scripts/**/*.js',
        images: './src/img/**/*',
    };
var config = {
    server: {
        baseDir: "./",
        index: "index.html"
    },
    tunnel: true,
    host: 'localhost',
    port: 3000,
    logPrefix: "Frontend_Devil"
};

gulp.task('server', function () {
    browserSync.init(config);
});

gulp.watch('**/*.php').on('change', function () {
    browserSync.reload();
});

gulp.task('clean', function () {
    return del(['./static/img']);
});

gulp.task('sass', function () {
    return sass(paths.styles, {style: 'compact'})
        .on('error', sass.logError)
        .pipe(sourceMaps.init({loadMaps: true}))
        .pipe(autoPrefix('last 2 version'))
        .pipe(cleanCSS())
        .pipe(sourceMaps.write())
        .pipe(rename({
            basename: "style",
        }))
        .pipe(gulp.dest('static/css/'))
        .pipe(reload({ stream:true }));
});

gulp.task('scripts', function () {
    return gulp.src(paths.scripts)
        .pipe(sourceMaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(sourceMaps.write())
        .pipe(gulp.dest('./static/js/'))
});

gulp.task('img', ['clean'], function () {
    return gulp.src(paths.images)
        .pipe(gulp.dest('./static/img/'));
});

gulp.task('watch', function () {
    gulp.watch(paths.all_style, ['sass']);
    gulp.watch(paths.scripts, ['scripts']);
});

gulp.task('default', ['sass', 'scripts', 'watch']);
